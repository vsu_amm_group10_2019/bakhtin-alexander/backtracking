﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabaBus
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();

            string[] strTimes = textBox1.Text.Split(' ');
            int j = 0;
            while (j < strTimes.Length && strTimes[j] != "")
                j++;
            if (j != strTimes.Length)
            {
                MessageBox.Show("Некорректный ввод", "Ошибка");
                return;
            }

            int[] arrivalTimes = new int[strTimes.Length];
            arrivalTimes[0] = Convert.ToInt32(strTimes[0]);
            for (int i = 1; i < strTimes.Length; i++)
                arrivalTimes[i] = arrivalTimes[i - 1] + Convert.ToInt32(strTimes[i]);

            if (arrivalTimes[arrivalTimes.Length - 1] >= 60)
            {
                MessageBox.Show("За автобусами наблюдали более часа", "Ошибка");
                return;
            }

            int minNumberOfBuses = arrivalTimes.Length;
            int[] resultIntervals = new int[minNumberOfBuses];
            int[] resultBusNumbers = new int[minNumberOfBuses];

            int[] intervals = new int[minNumberOfBuses];
            int[] firstArrivals = new int[minNumberOfBuses];
            int[] busNumber = new int[minNumberOfBuses];

            for (int i = 0; i < minNumberOfBuses; i++)
                busNumber[i] = -1;
            minNumberOfBuses++;

            getMinNumberOfBuses(ref arrivalTimes, 0, ref intervals, ref resultIntervals, 0, ref minNumberOfBuses, firstArrivals,
                busNumber, ref resultBusNumbers);

   
            //выводим минимальное количество автобусов
            textBox2.Text = minNumberOfBuses.ToString();

            //выводим интервалы автобусов
            for (int i = 0; i < minNumberOfBuses; i++)
                textBox3.Text += resultIntervals[i].ToString() + ' ';

            //выводим номера автобусов в порядке прибытия
            for (int i = 0; i < arrivalTimes.Length; i++)
                textBox4.Text += resultBusNumbers[i].ToString() + ' ';
        }


        /// <summary>
        /// ищем минимальное число автобусов, которыми можно покрыть введенные минуты
        /// </summary>
        /// <param name="arrivalTimes">  минуты, в которые приходили автобусы </param>
        /// <param name="currentTimePos"> порядковый номер рассматриваемой в текущий момент минуты </param>
        /// <param name="intervals"> назначенные автобусам интервалы </param>
        /// <param name="resultIntervals"> назначенные автобусам интервалы, которые образуют лучшее решение </param>
        /// <param name="currentNumberOfBuses"> текущее число автобусов </param>
        /// <param name="minNumberOfBuses"> минимальное число автобусов, которыми можно покрыть введенные минуты </param>
        /// <param name="firstArrivals"> установленные первые прибытия автобусов </param>
        /// <param name="busNumbers"> номер автобуса, пришедшего в i-м замере времени (текущее решение)</param>
        /// <param name="resultBusNumber"> номер автобуса, пришедшего в i-м замере времени (лучшее решение) </param>
        private void getMinNumberOfBuses(ref int[] arrivalTimes, int currentTimePos,
            ref int[] intervals, ref int[] resultIntervals,
            int currentNumberOfBuses, ref int minNumberOfBuses,
            int[] firstArrivals, int[] busNumbers, ref int[] resultBusNumber)
        {
            // если дошли до конца массива с минутами
            if (currentTimePos == arrivalTimes.Length)
            {
                // если полученное решение лучше имеющегося, обновляем имеющееся
                if (currentNumberOfBuses < minNumberOfBuses)
                {
                    minNumberOfBuses = currentNumberOfBuses;
                    for (int i = 0; i < currentNumberOfBuses; i++)
                        resultIntervals[i] = intervals[i];
                    for (int i = 0; i < currentTimePos; i++)
                        resultBusNumber[i] = busNumbers[i];
                }
                return;
            }
            // точно не получим более хороший ответ, возвращаемся к предыдущему состоянию
            if (currentNumberOfBuses == minNumberOfBuses)
                return;
            // рассмотрели не все минуты 
            if (busNumbers[currentTimePos] != -1) // в данную минуту пребывает какой-то автобус
            {
                // переходим в следующее состояние, не увеличивая число автобусов
                getMinNumberOfBuses(ref arrivalTimes, currentTimePos + 1, ref intervals, ref resultIntervals,
                    currentNumberOfBuses, ref minNumberOfBuses, firstArrivals, busNumbers, ref resultBusNumber);
            }
            else // данная минута не занята никаким автобусом
            {
                // считаем, что это первое пребытие автобуса
                firstArrivals[currentNumberOfBuses] = arrivalTimes[currentTimePos];
                intervals[currentNumberOfBuses] = 60; // интервал 60 говорит о том, что этот автобус не появляется повторно
                busNumbers[currentTimePos] = currentNumberOfBuses + 1; // +1, чтобы номера автобусов не с 0 начинались, а с 1
                // переходим в следующее состояние, увеличиваем число автобусов на 1, так как добавили новый
                getMinNumberOfBuses(ref arrivalTimes, currentTimePos + 1, ref intervals, ref resultIntervals,
                        currentNumberOfBuses + 1, ref minNumberOfBuses, firstArrivals, busNumbers, ref resultBusNumber);
                // убираем добавленный автобус, возвращаемся к предыдущему состоянию
                intervals[currentNumberOfBuses] = 60;
                busNumbers[currentTimePos] = -1;


                // пытаемся подцепить к предыдущим автобусам, которые появились лишь однажды
                int i = 0;
                // пока не дошли до нашего автобуса 
                while (i < currentNumberOfBuses)
                {
                    // если автобус в минуту с индексом i появлялся лишь однажды
                    // и минута первого появления автобуса меньше интервала (текущий момент минус первое появление)
                    // а также в будущем в минуты, которые он должен прийти, он действительно приходит
                    if (intervals[i] == 60 && firstArrivals[i] < arrivalTimes[currentTimePos] - firstArrivals[i] &&
                        CanBeTheSameBus(arrivalTimes, ref busNumbers, arrivalTimes[currentTimePos] - arrivalTimes[i], currentTimePos))
                    {
                        // устанавливаем интервал (текущий момент минус первое появление)
                        intervals[i] = arrivalTimes[currentTimePos] - arrivalTimes[i];
                        // резервируем за данным автобусов появления в будущем
                        TakeSpots(arrivalTimes, ref busNumbers, intervals[i], currentTimePos, busNumbers[i]);
                        // переходим в следующее состояние, количество автобусов не увеличиваем, так как автобус пришел повторно, он уже учтен
                        getMinNumberOfBuses(ref arrivalTimes, currentTimePos + 1, ref intervals, ref resultIntervals,
                            currentNumberOfBuses, ref minNumberOfBuses, firstArrivals, busNumbers, ref resultBusNumber);
                        // возвращаемся в исходное состояние
                        // освобождаем моменты в будущем, зарезервированные под автобус
                        FreeSpots(ref busNumbers, currentTimePos, busNumbers[currentTimePos]);
                        intervals[i] = 60;
                    }
                    i++;
                }
            }
        }

        // смотрим есть ли в будущем появления этого автобуса,
        // если нет хотя бы одного появления, то данный автобус не может существовать
        // index - номер в массиве arrivalTimes, по которому находится первое появление автобуса
        bool CanBeTheSameBus(int[] arrivalTimes, ref int[] busNumbers, int interval, int index)
        {
            int nextTime = arrivalTimes[index];
            while (index < arrivalTimes.Length && arrivalTimes[index] <= nextTime)
            {
                if (arrivalTimes[index] == nextTime && busNumbers[index] == -1)
                    nextTime += interval;
                index++;
            }
            return nextTime >= 60;
        }

        // резервируем за автобусом минуты
        // index - номер, с которого начинаем резервировать минуты
        void TakeSpots(int[] arrivalTimes, ref int[] busNumbers, int interval, int index, int busNumber)
        {
            int nextTime = arrivalTimes[index];
            while (index < arrivalTimes.Length)
            {
                if (arrivalTimes[index] == nextTime && busNumbers[index] == -1)
                {
                    busNumbers[index] = busNumber;
                    nextTime += interval;
                }
                index++;
            }
        }
        // освобождаем минуты, занятые автобусом
        // index - номер, с которого начинаем освоождать минуты
        void FreeSpots(ref int[] busNumbers, int index, int busNumber)
        {
            for (; index < busNumbers.Length; index++)
                if (busNumbers[index] == busNumber)
                    busNumbers[index] = -1;
        }


        // ограничиваем символы, которые можно вводить в текст-бокс
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // разрешаем в текст-бокс вводить цифры и пробелы
            // e.keyChar == 8 позволяет стирать введенное
            e.Handled = !(Char.IsDigit(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar) || e.KeyChar == 8);
        }
    }
}
